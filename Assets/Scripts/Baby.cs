﻿namespace BabySimulator
{
    using System.Collections.Generic;
    using UnityEngine;
    using UniRx;

    using UI;
    using System;

    public class Baby
    {
        #region Private Members
        /// <summary>
        /// A dictionary of double? attributes for the baby
        /// </summary>
        private readonly Dictionary<BabyAttribute, float> _attributes = new Dictionary<BabyAttribute, float>();

        /// <summary>
        /// A dictionary of boolean flags indicating whether the given attribute is active
        /// </summary>
        private readonly Dictionary<BabyAttribute, bool> _isActivated = new Dictionary<BabyAttribute, bool>();

        /// <summary>
        /// A dictionary of boolean flags indicating whether the given attribute is active
        /// </summary>
        private readonly Dictionary<BabyAttribute, float> _baseGrowthRate = new Dictionary<BabyAttribute, float>();

        /// <summary>
        /// A dictionary of reactive UI properties
        /// </summary>
        private readonly Dictionary<BabyAttribute, FloatReactiveProperty> _uiReactiveProps = new Dictionary<BabyAttribute, FloatReactiveProperty>();

        /// <summary>
        /// A predicate returning true when the baby has failed
        /// </summary>
        private Predicate<Baby> _isFailed;

        /// <summary>
        /// A predicate returning true when the baby is successful
        /// </summary>
        private Predicate<Baby> _isSucceeded;
        #endregion

        #region Constructors
        /// <summary>
        /// Default constructor
        /// </summary>
        public Baby()
        {
            SetupAttributes();

            BabyUIBridge.Target = this;

            // default conditions
            SetAttributeValue(BabyAttribute.Wakefulness, 1f);

            // default failure conditions
            _isFailed = (b) =>
            {
                return _attributes[BabyAttribute.Hunger] >= 1f
                    || _attributes[BabyAttribute.Boredom] >= 1f;
            };

            // default success conditions
            _isSucceeded = (b) =>
            {
                return GetAttributeValue(BabyAttribute.Wakefulness) < 0.05f;
            };
        }
        
        /// <summary>
        /// Constructor to create baby with existing attributes
        /// </summary>
        public Baby(float hunger, float wakefulness, float boredom, float distress) : this()
        {
            SetAttributeValue(BabyAttribute.Hunger, hunger);
            SetAttributeValue(BabyAttribute.Wakefulness, wakefulness);
            SetAttributeValue(BabyAttribute.Boredom, boredom);
            SetAttributeValue(BabyAttribute.Distress, distress);
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Apply a penalty
        /// </summary>
        /// <param name="itemType"></param>
        internal void ApplyPenalty(BabyFunction itemType, bool asBenefit = false)
        {
            if (itemType == BabyFunction.Feed)
            {
                IncrementAttribute(BabyAttribute.Hunger, 0.03f * (asBenefit ? -1f : 1f));
            } 
            else if (itemType == BabyFunction.Rock)
            {
                IncrementAttribute(BabyAttribute.Wakefulness, 0.03f * (asBenefit ? -1f : 1f));
            }
            else if (itemType == BabyFunction.Shhh)
            {
                IncrementAttribute(BabyAttribute.Distress, 0.03f * (asBenefit ? -1f : 1f));
            }
            else
            {
                IncrementAttribute(BabyAttribute.Boredom, 0.03f * (asBenefit ? -1f : 1f));
            }
        }

        /// <summary>
        /// Set the predicate conditions of success/failure for the baby
        /// </summary>
        /// <param name="success"></param>
        /// <param name="failure"></param>
        public void SetConditions(Predicate<Baby> success, Predicate<Baby> failure)
        {
            _isSucceeded = success ?? _isSucceeded;
            _isFailed = failure ?? _isFailed;
        }

        /// <summary>
        /// Activates all the attributes on the baby
        /// </summary>
        public void ActivateAll()
        {
            Activate(BabyAttribute.Hunger);
            Activate(BabyAttribute.Wakefulness);
            Activate(BabyAttribute.Boredom);
            Activate(BabyAttribute.Distress);
        }

        /// <summary>
        /// Updates the baby's game state
        /// </summary>
        public void Update()
        {
            if (_attributes[BabyAttribute.Hunger] > 0.6f)
            {
                IncrementAttribute(BabyAttribute.Wakefulness, 0.01f);
            }

            if (_attributes[BabyAttribute.Boredom] > 0.6f)
            {
                IncrementAttribute(BabyAttribute.Wakefulness, 0.01f);
            }
        }

        /// <summary>
        /// Activates a given attribute. Pass a second argument 'false' to deactivate
        /// </summary>
        /// <param name="attribute"></param>
        /// <param name="value"></param>
        public void Activate(BabyAttribute attribute, bool value = true)
        {
            _isActivated[attribute] = value;
        }

        /// <summary>
        /// Returns whether or not the given attribute is active
        /// </summary>
        /// <param name="attribute"></param>
        /// <returns></returns>
        public bool IsActive(BabyAttribute attribute)
        {
            return _isActivated[attribute];
        }

        /// <summary>
        /// Check if the baby has failed
        /// </summary>
        /// <returns></returns>
        public bool IsFailed()
        {
            return _isFailed(this);
        }

        /// <summary>
        /// Checks if the baby is successful
        /// </summary>
        /// <returns></returns>
        public bool IsSucceeded()
        {
            return _isSucceeded(this);
        }
        
        /// <summary>
        /// Feeds the baby, which reduces hunger but increases boredom
        /// </summary>
        public void Feed()
        {
            IncrementAttribute(BabyAttribute.Hunger, -0.01f);
            IncrementAttribute(BabyAttribute.Boredom, 0.005f);
        }

        /// <summary>
        /// Shushes the baby, which reduces distress but increases fatigue
        /// </summary>
        public void Shhh()
        {
            IncrementAttribute(BabyAttribute.Distress, -0.01f);
        }

        /// <summary>
        /// Rocks the baby, which reduces wakefulness but increases hunger
        /// </summary>
        public void Rock()
        {
            IncrementAttribute(BabyAttribute.Wakefulness, -0.01f);
            IncrementAttribute(BabyAttribute.Hunger, 0.005f);
        }

        /// <summary>
        /// Tickles the baby, which reduces boredom but increases fatigue
        /// </summary>
        public void Tickle()
        {
            IncrementAttribute(BabyAttribute.Boredom, -0.01f);
        }
        #endregion

        #region Private Methods
        /// <summary>
        /// Updates an attribute with the given growth rates
        /// </summary>
        /// <param name="attribute"></param>
        private void UpdateAttribute(BabyAttribute attribute)
        {
            IncrementAttribute(attribute, _baseGrowthRate[attribute]);
        }

        /// <summary>
        /// Updates an attribute by a given amount
        /// </summary>
        /// <param name="attribute"></param>
        /// <param name="value"></param>
        private void IncrementAttribute(BabyAttribute attribute, float? value)
        {
            var newValue = Mathf.Clamp01((value ?? 0) + _attributes[attribute]);
            SetAttributeValue(attribute, newValue);
        }

        /// <summary>
        /// Sets up default attributes for the baby
        /// </summary>
        private void SetupAttributes()
        {
            _attributes.Add(BabyAttribute.Hunger, 0);
            _attributes.Add(BabyAttribute.Wakefulness, 0);
            _attributes.Add(BabyAttribute.Boredom, 0);
            _attributes.Add(BabyAttribute.Distress, 0);
            
            _baseGrowthRate.Add(BabyAttribute.Hunger, 0.01f);
            _baseGrowthRate.Add(BabyAttribute.Wakefulness, 0.01f);
            _baseGrowthRate.Add(BabyAttribute.Boredom, 0.01f);
            _baseGrowthRate.Add(BabyAttribute.Distress, 0);
            
            _isActivated.Add(BabyAttribute.Hunger, false);
            _isActivated.Add(BabyAttribute.Wakefulness, false);
            _isActivated.Add(BabyAttribute.Boredom, false);
            _isActivated.Add(BabyAttribute.Distress, false);
            
            _uiReactiveProps.Add(BabyAttribute.Hunger, new FloatReactiveProperty());
            _uiReactiveProps.Add(BabyAttribute.Wakefulness, new FloatReactiveProperty());
            _uiReactiveProps.Add(BabyAttribute.Boredom, new FloatReactiveProperty());
            _uiReactiveProps.Add(BabyAttribute.Distress, new FloatReactiveProperty());
        }

        /// <summary>
        /// Gets an attribute value, returning null if the attribute is not active
        /// </summary>
        /// <param name="attribute"></param>
        /// <returns></returns>
        private float? GetAttributeValue(BabyAttribute attribute)
        {
            return _isActivated[attribute] ? (float?)_attributes[attribute] : null;
        }

        /// <summary>
        /// Sets an attribute value
        /// </summary>
        /// <param name="attribute"></param>
        /// <param name="value"></param>
        private void SetAttributeValue(BabyAttribute attribute, float? value)
        {
            _attributes[attribute] = Mathf.Clamp01(value ?? 0);
            _uiReactiveProps[attribute].Value = _attributes[attribute];
        }
        #endregion

        #region Public Properties
        /// <summary>
        /// Gets the Hunger attribute of the baby
        /// </summary>
        public float? Hunger
        {
            get
            {
                return GetAttributeValue(BabyAttribute.Hunger);
            }
            private set
            {
                SetAttributeValue(BabyAttribute.Hunger, value);
            }
        }

        /// <summary>
        /// Gets the Wakefulness attribute of the baby
        /// </summary>
        public float? Wakefulness
        {
            get
            {
                return GetAttributeValue(BabyAttribute.Wakefulness);
            }
            private set
            {
                SetAttributeValue(BabyAttribute.Wakefulness, value);
            }
        }

        /// <summary>
        /// Gets the Boredom attribute of the baby
        /// </summary>
        public float? Boredom
        {
            get
            {
                return GetAttributeValue(BabyAttribute.Boredom);
            }
            private set
            {
                SetAttributeValue(BabyAttribute.Boredom, value);
            }
        }

        /// <summary>
        /// Gets the Distress attribute of the baby
        /// </summary>
        public float? Distress
        {
            get
            {
                return GetAttributeValue(BabyAttribute.Distress);
            }
            private set
            {
                SetAttributeValue(BabyAttribute.Distress, value);
            }
        }

        /// <summary>
        /// Gets a flag indicating whether the baby has become a MONSTER and you have failed and will never 
        /// ever ever ever sleep again. 
        /// </summary>
        public bool IsMonster
        {
            get
            {
                return _isFailed(this);
            }
        }
        
        /// <summary>
        /// Gets a reactive property bound to the hunger of the baby
        /// </summary>
        public FloatReactiveProperty UIHunger
        {
            get
            {
                return _uiReactiveProps[BabyAttribute.Hunger];
            }
        }

        /// <summary>
        /// Gets a reactive property bound to the distress of the baby
        /// </summary>
        public FloatReactiveProperty UIDistress
        {
            get
            {
                return _uiReactiveProps[BabyAttribute.Distress];
            }
        }

        /// <summary>
        /// Gets a reactive property bound to the wakefulness of the baby
        /// </summary>
        public FloatReactiveProperty UIWakefulness
        {
            get
            {
                return _uiReactiveProps[BabyAttribute.Wakefulness];
            }
        }

        /// <summary>
        /// Gets a reactive property bound to the boredom of the baby
        /// </summary>
        public FloatReactiveProperty UIBoredom
        {
            get
            {
                return _uiReactiveProps[BabyAttribute.Boredom];
            }
        }
        #endregion
    }
}