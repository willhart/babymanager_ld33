﻿namespace BabySimulator
{
    using DG.Tweening;
    using UnityEngine;
    using System.Collections.Generic;
    using System.Linq;
    using System;

    public class SpawnItemExitManager : MonoBehaviour
	{
        #region Private Members
        private List<TargetItem> _targetItems = new List<TargetItem>();

        private object locker = new object();
		#endregion

		#region Public Members
        public void OnTriggerEnter2D(Collider2D collider)
        {
            var target = collider.GetComponent<TargetItem>();
            if (target == null) return;
            _targetItems.Add(target);

            target.transform.DOScale(1.1f, 0.2f);
        }

        public void OnTriggerExit2D(Collider2D collider)
        {
            _targetItems.Remove(collider.GetComponent<TargetItem>());
        }
		#endregion

		#region Constructors
		#endregion

		#region Public Methods
        public void RemoveItem(BabyFunction fn)
        {
            TargetItem item;

            lock(locker)
            {
                if (!_targetItems.Any(o => o.ItemType == fn))
                    return;

                item = _targetItems.First(o => o.ItemType == fn);
                _targetItems.Remove(item);
            }

            item.KillItem(transform, true);
                SpawnRemover.CurrentBaby.ApplyPenalty(item.ItemType, true);
        }
		#endregion

		#region Private Methods
		#endregion

		#region Public Properties
		#endregion
	}
}