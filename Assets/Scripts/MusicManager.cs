﻿namespace BabySimulator
{
    using System;
    using UnityEngine;

    using UniRx;
    using UniRx.Triggers;

    public class MusicManager : MonoBehaviour
	{
        #region Private Members
        [SerializeField]
        private AudioSource track01;

        [SerializeField]
        private AudioSource track02;

        [SerializeField]
        private AudioSource track03;
        #endregion

        #region Public Members
        #endregion

        #region Constructors
        void Awake()
        {
            track01.volume = 0.5f;
            track02.volume = 0f;
            track03.volume = 0f;

            this.UpdateAsObservable()
                .Buffer(TimeSpan.FromSeconds(0.2f))
                .Subscribe(_ =>
                {
                    var b = SpawnRemover.CurrentBaby;

                    if (b.Boredom > 0.3 || b.Distress > 0.3 || b.Hunger > 0.3)
                    {
                        track02.volume = Math.Min(0.2f, track02.volume + 0.008f);
                    }
                    else
                    {
                        track02.volume = Mathf.Max(0f, track02.volume - 0.008f);
                    }

                    if (b.Boredom > 0.5 || b.Distress > 0.5 || b.Hunger > 0.5)
                    {
                        track03.volume = Math.Min(0.2f, track03.volume + 0.008f);
                    }
                    else
                    {
                        track03.volume = Mathf.Max(0f, track03.volume - 0.008f);
                    }
                })
                .AddTo(this);
        }
        #endregion

        #region Public Methods
        #endregion

        #region Private Methods
        #endregion

        #region Public Properties
        #endregion
    }
}