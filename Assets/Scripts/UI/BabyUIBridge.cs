﻿namespace BabySimulator.UI
{
    using UnityEngine;
    using UnityEngine.UI;
    using UniRx;

	public static class BabyUIBridge
	{
        #region Private Members
        private static Baby _baby;
        private static CompositeDisposable _disposables = new CompositeDisposable();
        #endregion

        #region Public Members
        public static Text HungerText;
        public static Text WakefulnessText;
        public static Text BoredomText;
        public static Text DistressText;
        
        public static BoolReactiveProperty FeedActive = new BoolReactiveProperty();
        public static BoolReactiveProperty RockActive = new BoolReactiveProperty();
        public static BoolReactiveProperty ShhhActive = new BoolReactiveProperty();
        public static BoolReactiveProperty TickleActive = new BoolReactiveProperty();

        public static BoolReactiveProperty IsRunning = new BoolReactiveProperty(false);
        #endregion

        #region Public Properties

        /// <summary>
        /// Sets the baby which is the target for the UI bridge
        /// </summary>
        public static Baby Target
        {
            set
            {
                _disposables.Clear(); // clear old bindings
                _disposables = new CompositeDisposable();

                _baby = value;

                if (HungerText != null)
                    _baby.UIHunger.SubscribeToText(HungerText, x => Mathf.Round(x * 100).ToString() + "%").AddTo(_disposables);

                if (WakefulnessText != null)
                    _baby.UIWakefulness.SubscribeToText(WakefulnessText, x => Mathf.Round(x * 100).ToString() + "%").AddTo(_disposables);

                if (BoredomText != null)
                    _baby.UIBoredom.SubscribeToText(BoredomText, x => Mathf.Round(x * 100).ToString() + "%").AddTo(_disposables);

                if (DistressText != null)
                    _baby.UIDistress.SubscribeToText(DistressText, x => Mathf.Round(x * 100).ToString() + "%").AddTo(_disposables);
                
                FeedActive.Value = _baby.IsActive(BabyAttribute.Hunger);
                RockActive.Value = _baby.IsActive(BabyAttribute.Wakefulness);
                ShhhActive.Value = _baby.IsActive(BabyAttribute.Distress);
                TickleActive.Value = _baby.IsActive(BabyAttribute.Boredom);
            }
        }
		#endregion
	}
}