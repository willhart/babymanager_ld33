﻿ namespace BabySimulator.UI
{
    using UnityEngine;

    public class MainMenuManager : MonoBehaviour
	{
        #region Private Members
		#endregion

		#region Public Members
		#endregion

		#region Constructors
		#endregion

		#region Public Methods
        public void ExitApplication()
        {
            Debug.Log("Quitting");
            Application.Quit();
        }

        public void StartCampaign()
        {
            Debug.Log("Loading Campaign");
            GameObject.Find("GameState").GetComponent<CampaignManager>().SetupTutorial();
            Application.LoadLevel("Sandbox");
        }

        public void StartFreePlay()
        {
            Debug.Log("Loading Free Play");
            GameObject.Find("GameState").GetComponent<CampaignManager>().ClearLevels();
            Application.LoadLevel("Sandbox");
        }
		#endregion

		#region Private Methods
		#endregion

		#region Public Properties
		#endregion
	}
}