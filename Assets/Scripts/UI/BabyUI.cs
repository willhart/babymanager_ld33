﻿namespace BabySimulator.UI
{
    using UnityEngine;
    using UnityEngine.UI;

    using UniRx;

	public class BabyUI : MonoBehaviour
	{
        #region Private Members
        [SerializeField]
        private Text HungerText;

        [SerializeField]
        private Text WakefulnessText;

        [SerializeField]
        private Text BoredomText;

        [SerializeField]
        private Text DistressText;

        [SerializeField]
        private Text HungerLabel;

        [SerializeField]
        private Text WakefulnessLabel;

        [SerializeField]
        private Text BoredomLabel;

        [SerializeField]
        private Text DistressLabel;

        [SerializeField]
        private GameObject FeedIndicator;

        [SerializeField]
        private GameObject RockIndicator;

        [SerializeField]
        private GameObject ShhhIndicator;

        [SerializeField]
        private GameObject TickleIndicator;
        #endregion

        #region Public Members
        #endregion

        #region Constructors
        void Awake()
        {
            BabyUIBridge.HungerText = HungerText;
            BabyUIBridge.WakefulnessText = WakefulnessText;
            BabyUIBridge.BoredomText = BoredomText;
            BabyUIBridge.DistressText = DistressText;
            
            BabyUIBridge.FeedActive.Subscribe(o => {
                FeedIndicator.SetActive(o);
                HungerText.gameObject.SetActive(o);
                HungerLabel.gameObject.SetActive(o);
            });

            BabyUIBridge.RockActive.Subscribe(o => {
                RockIndicator.SetActive(o);
                WakefulnessText.gameObject.SetActive(o);
                WakefulnessLabel.gameObject.SetActive(o);
            });

            BabyUIBridge.ShhhActive.Subscribe(o => {
                ShhhIndicator.SetActive(o);
                DistressText.gameObject.SetActive(o);
                DistressLabel.gameObject.SetActive(o);
            });

            BabyUIBridge.TickleActive.Subscribe(o => {
                TickleIndicator.SetActive(o);
                BoredomText.gameObject.SetActive(o);
                BoredomLabel.gameObject.SetActive(o);
            });
        }

		#endregion

		#region Public Methods
		#endregion

		#region Private Methods
		#endregion

		#region Public Properties
		#endregion
	}
}