﻿namespace BabySimulator.UI
{
    using UnityEngine;
    using UnityEngine.UI;
    
    using System.Collections;

    public class DisplayUserText : MonoBehaviour
	{
        #region Private Members
        [SerializeField]
        private GameObject displayPanel;

        [SerializeField]
        private Text messageText;

        private GameEngine _caller;
        private bool _start;
		#endregion

		#region Public Members
        public void DisplayMessage(string text, GameEngine caller, bool start)
        {
            messageText.text = text;
            displayPanel.SetActive(true);
            _caller = caller;
            _start = start;

            StartCoroutine(HidePanel());
        }
		#endregion

		#region Constructors
		#endregion

		#region Public Methods
		#endregion

		#region Private Methods
        private IEnumerator HidePanel()
        {
            yield return new WaitForSeconds(6);
            displayPanel.SetActive(false);
            _caller.RunGame(_start);
        }
		#endregion

		#region Public Properties
		#endregion
	}
}