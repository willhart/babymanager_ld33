﻿namespace BabySimulator
{
    public enum BabyAttribute
    {
        Hunger = 0,
        Wakefulness,
        Boredom,
        Distress
    }
}