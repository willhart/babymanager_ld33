﻿namespace BabySimulator
{
    using UnityEngine;
    using UniRx;
    using UniRx.Triggers;

    using UI;
    using System;

    public class GameEngine : MonoBehaviour
	{
        #region Private Members
        [SerializeField]
        private DisplayUserText _messageDisplay;

        [SerializeField]
        private SpawnItemExitManager _itemExit;

        private Baby _baby;
        private BabyLevelDescription _thisLevel;
        private bool first = true;
        private CompositeDisposable _babyObservableDisposer = new CompositeDisposable();
        #endregion


        #region Constructors
        void Awake()
        {
            this.UpdateAsObservable()
                .Buffer(TimeSpan.FromSeconds(0.5))
                .Subscribe(_ =>
                {
                    if (BabyUIBridge.IsRunning.Value)
                    {
                        _baby.Update();

                        if (_baby.IsFailed())
                        {
                            HandleCompletion(false);
                        }
                        else if (_baby.IsSucceeded())
                        {
                            HandleCompletion(true);
                        }
                    }
                });

            SetupLevel();
        }
        #endregion

        #region Public Methods
        public void RunGame(bool start)
        {
            BabyUIBridge.IsRunning.Value = start;

            if (!BabyUIBridge.IsRunning.Value)
            {
                SetupLevel();
            }
        }
        #endregion

        #region Private Methods
        /// <summary>
        /// Handles failure or success of a level
        /// </summary>
        private void HandleCompletion(bool success)
        {
            BabyUIBridge.IsRunning.Value = false;

            if (_thisLevel != null && _thisLevel.AfterMessage != string.Empty)
            {
                _messageDisplay.DisplayMessage(_thisLevel.AfterMessage, this, false);
                //Debug.Log(_thisLevel.AfterMessage);
            }
        }

        /// <summary>
        /// Sets up a new level
        /// </summary>
        private void SetupLevel()
        {
            BabyUIBridge.IsRunning.Value = false;

            try
            {
                var babyGeneratorAKAMummy = GameObject.Find("GameState").GetComponent<CampaignManager>();
                _thisLevel = babyGeneratorAKAMummy.GetNextLevel();
            }
            catch (NullReferenceException)
            {
                _thisLevel = null;
            }

            if (_thisLevel == null)
            {
                if (first)
                {
                    _baby = new Baby();
                    _baby.ActivateAll();
                    BabyUIBridge.IsRunning.Value = true;
                }
                else
                {
                    Application.LoadLevel("MainMenu");
                }
            }
            else
            {
                _baby = _thisLevel.ToBaby();
                if (_thisLevel != null && _thisLevel.BeforeMessage != string.Empty)
                {
                    Debug.Log(_thisLevel.BeforeMessage);
                    
                    _messageDisplay.DisplayMessage(_thisLevel.BeforeMessage, this, true);
                }
            }

            first = false;
            BabyUIBridge.Target = _baby;
            SpawnRemover.CurrentBaby = _baby;

            // recreate key bindings
            _babyObservableDisposer.Clear();
            ObserveKey(KeyCode.Q, (b) => { return BabyUIBridge.IsRunning.Value && b.IsActive(BabyAttribute.Hunger); }, () => {
                _itemExit.RemoveItem(BabyFunction.Feed); });
            ObserveKey(KeyCode.W, (b) => { return BabyUIBridge.IsRunning.Value && b.IsActive(BabyAttribute.Wakefulness); }, () => {
                _itemExit.RemoveItem(BabyFunction.Rock); });
            ObserveKey(KeyCode.E, (b) => {
                return BabyUIBridge.IsRunning.Value && b.IsActive(BabyAttribute.Distress); }, () => {
                _itemExit.RemoveItem(BabyFunction.Shhh); });
            ObserveKey(KeyCode.R, (b) => { return BabyUIBridge.IsRunning.Value && b.IsActive(BabyAttribute.Boredom); }, () => {
                _itemExit.RemoveItem(BabyFunction.Tickle); });
        }

        /// <summary>
        /// Creates a key press observer
        /// </summary>
        /// <param name="code"></param>
        /// <param name="doThis"></param>
        private void ObserveKey(KeyCode code, Predicate<Baby> condition, Action doThis)
        {
            this.FixedUpdateAsObservable()
                .Where(_ => Input.GetKeyDown(code) && condition(_baby))
                .Subscribe(_ => doThis())
                .AddTo(_babyObservableDisposer);
        }
        #endregion

        #region Public Properties
        #endregion
    }
}