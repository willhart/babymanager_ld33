﻿namespace BabySimulator
{
    using DG.Tweening;
    using UnityEngine;

	public class TargetItem : MonoBehaviour
	{
        #region Private Members
        [SerializeField]
        private AudioSource usedNoise;

        private Tweener _initialTween;
        #endregion

        #region Public Members
        public BabyFunction ItemType;
        #endregion

        #region Constructors
        void Awake()
        {
            _initialTween = transform.DOMove(new Vector3(-40f, 0), Random.Range(6f, 15f)).SetRelative();


        }
        #endregion

        #region Public Methods
        public void KillItem(Transform target, bool success = false)
        {
            if (success)
            {
                _initialTween.Kill();

                try
                {
                    transform.DOMove(target.position, 0.4f);
                    transform.DOScale(0.1f, 0.4f);
                    usedNoise.Play();
                }
                catch (MissingReferenceException)
                {
                    // just throw this away. Yeah not pretty but only 48hrs
                }
            }

            Destroy(gameObject, 0.4f);
        }
        #endregion

        #region Private Methods
        #endregion

        #region Public Properties
        #endregion
    }
}