﻿namespace BabySimulator
{
    using System.Collections.Generic;

	public static class CampaignFactory
	{
		#region Public Methods
        public static List<BabyLevelDescription> Construct()
        {
            return new List<BabyLevelDescription>()
            {
                new BabyLevelDescription(
                    "Owning a baby is hard work, but I'll give you a soft start. First of all you need to make sure they sleep enough. Press W at the right times to rock your baby to sleep.",
                    "Well done, the baby went off to sleep, we'll make a parent of you yet!",
                    -1f, 0.3f, -1f, -1f,
                    (b) => { return false; },
                    (b) => { return b.Wakefulness < 0.05f; }),

                new BabyLevelDescription(
                    "Its not all about sleep though. Sometimes you have to feed the baby. Feed and rock the baby until its is ready for sleep.",
                    "Ok, you may be getting the hang of it. Your baby ate and played until it fell asleep.",
                    0.3f, 0.3f, -1f, -1f,
                    (b) => { return false; },
                    (b) => { return b.Wakefulness < 0.05f; }
                ),

                new BabyLevelDescription(
                    "Let's see what you are made of. We'll try the full baby experience - feed, shhh, tickle and rock your baby to sleep.",
                    "Success, ok maybe I underestimated you. It's possible you may have the right stuff...",
                    0f, 0.5f, 0f, 0f,
                    (b) => { return false; },
                    (b) => { return b.Wakefulness< 0.05f; }
                ),

                new BabyLevelDescription(
                    "You know, they say with love and a good heart you can't mess up a baby too badly. They were wrong. Do nothing and see what happens",
                    "You monster! How could you treat a little child like that?",
                    0.5f, 0.5f, 0.5f, 0f,
                    (b) => { return false; },
                    (b) => { return b.Distress >= 1f; }
                ),

                new BabyLevelDescription(
                    "Ok, I think you've had enough time with the training wheels on. Time to get to a real baby! Get your baby to sleep without it turning into a monster, and remember.... have fun!",
                    "You have succeeded. You aren't a (totally) terrible parent.",
                    0, 1f, 0, 0)
            };
        }
		#endregion
	}
}