﻿namespace BabySimulator
{
    using System;
    using System.Collections.Generic;
    using UnityEngine;

    using UniRx;
    using UniRx.Triggers;

    using UI;

	public class ObjectSpawner : MonoBehaviour
	{
        #region Private Members
        [SerializeField]
        private GameObject QSpawn;

        [SerializeField]
        private GameObject WSpawn;

        [SerializeField]
        private GameObject ESpawn;

        [SerializeField]
        private GameObject RSpawn;
        
        [SerializeField]
        private Transform SpawnLocation;

        private readonly List<GameObject> ObjectPrefabs = new List<GameObject>();
        #endregion

        #region Public Members
        #endregion

        #region Constructors
        void Awake()
        {
            this.UpdateAsObservable()
                .Buffer(TimeSpan.FromSeconds(0.1))
                .Where(_ => UnityEngine.Random.Range(0f, 1f) < 0.1f && BabyUIBridge.IsRunning.Value)
                .Subscribe(_ =>
                {
                    if (ObjectPrefabs.Count == 0) return;
                    var pos = new Vector3(SpawnLocation.position.x, SpawnLocation.position.y + UnityEngine.Random.Range(-1.5f, 1.5f));
                    Instantiate(ObjectPrefabs[UnityEngine.Random.Range(0, ObjectPrefabs.Count)], pos, Quaternion.identity);
                })
                .AddTo(this);

            ManageSpawnAvailability(BabyUIBridge.FeedActive, QSpawn);
            ManageSpawnAvailability(BabyUIBridge.RockActive, WSpawn);
            ManageSpawnAvailability(BabyUIBridge.ShhhActive, ESpawn);
            ManageSpawnAvailability(BabyUIBridge.TickleActive, RSpawn);
        }
        #endregion

        #region Public Methods
        #endregion

        #region Private Methods
        /// <summary>
        /// Subscribe to the availability of specific items
        /// </summary>
        /// <param name="prop"></param>
        /// <param name="spawn"></param>
        private void ManageSpawnAvailability(BoolReactiveProperty prop, GameObject spawn)
        {
            prop.Subscribe(v =>
            {
                if (v)
                {
                    ObjectPrefabs.Add(spawn);
                }
                else
                {
                    ObjectPrefabs.Remove(spawn);
                }
            })
            .AddTo(this);
        }
        #endregion

        #region Public Properties
        #endregion
    }
}