﻿namespace BabySimulator
{
    using System;

    public class BabyLevelDescription
    {
        private Predicate<Baby> _isFailed;
        private Predicate<Baby> _isSucceeded;

        public BabyLevelDescription(string before, string after, float hunger, float wakefulness, float boredom, float distress)
        {
            BeforeMessage = before;
            AfterMessage = after;
            
            StartingHunger = hunger;
            StartingWakefulness = wakefulness;
            StartingBoredom = boredom;
            StartingDistress = distress;
        }
        
        public BabyLevelDescription(string before, string after, float hunger, float wakefulness, float boredom, float distress, Predicate<Baby> isFailed, Predicate<Baby> isSucceeded) 
            : this(before, after, hunger, wakefulness, boredom, distress)
        {
            _isFailed = isFailed;
            _isSucceeded = isSucceeded;
        }

        public Baby ToBaby()
        {
            var b = new Baby(StartingHunger, StartingWakefulness, StartingBoredom, StartingDistress);
            
            if (HungerActive) b.Activate(BabyAttribute.Hunger);
            if (WakefulnessActive) b.Activate(BabyAttribute.Wakefulness);
            if (BoredomActive) b.Activate(BabyAttribute.Boredom);
            if (DistressActive) b.Activate(BabyAttribute.Distress);

            b.SetConditions(_isSucceeded, _isFailed);

            return b;
        }

        public string BeforeMessage { get; private set; }
        public string AfterMessage { get; private set; }
        public float StartingHunger { get; private set; }
        public float StartingWakefulness { get; private set; }
        public float StartingBoredom { get; private set; }
        public float StartingDistress { get; private set; }
        public bool HungerActive { get { return StartingHunger >= 0; } }
        public bool WakefulnessActive { get { return StartingWakefulness >= 0; } }
        public bool BoredomActive { get { return StartingBoredom >= 0; } }
        public bool DistressActive { get { return StartingDistress >= 0; } }
    }
}
