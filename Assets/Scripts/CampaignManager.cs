﻿namespace BabySimulator
{
    using System.Collections.Generic;
	using UnityEngine;

	public class CampaignManager : MonoBehaviour
	{
        #region Private Members
        private static GameObject _instance;

        private readonly List<BabyLevelDescription> _levels = new List<BabyLevelDescription>();
		#endregion

		#region Public Members
		#endregion

		#region Constructors
        void Awake()
        {
            if (_instance != null)
            {
                Destroy(gameObject);
                return;
            }

            _instance = gameObject;
            DontDestroyOnLoad(gameObject);
        }
		#endregion

		#region Public Methods
        /// <summary>
        /// Gets the next level in a tutorial sequence, or returns null if it should be free play
        /// </summary>
        /// <returns></returns>
        public BabyLevelDescription GetNextLevel()
        {
            if (_levels.Count == 0)
            {
                return null;
            }

            var nextBaby = _levels[0];
            _levels.RemoveAt(0);

            return nextBaby;
        }

        /// <summary>
        /// Clears all current levels
        /// </summary>
        public void ClearLevels()
        {
            _levels.Clear();
        }

        /// <summary>
        /// 
        /// </summary>
        public void SetupTutorial()
        {
            ClearLevels();
            _levels.AddRange(CampaignFactory.Construct());
        }
		#endregion

		#region Private Methods
		#endregion

		#region Public Properties
		#endregion
	}
}