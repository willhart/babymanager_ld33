﻿namespace BabySimulator
{
	using UnityEngine;

    using UI;

	public class SpawnRemover : MonoBehaviour
	{
        #region Private Members
        [SerializeField]
        private Transform FeedItem;

        [SerializeField]
        private Transform RockItem;

        [SerializeField]
        private Transform ShhhItem;

        [SerializeField]
        private Transform TickleItem;
        #endregion

        #region Public Members
        public static Baby CurrentBaby;
        #endregion

        #region Constructors
        #endregion

        #region Public Methods
        /// <summary>
        /// Remove spawn and add score to the baby
        /// </summary>
        /// <param name="other"></param>
        public void OnTriggerEnter2D(Collider2D c)
        {
            var target = c.GetComponent<TargetItem>();
            if (target == null) return;

            Transform targetTransform = null;

            if (target.ItemType == BabyFunction.Feed)
            {
                targetTransform = FeedItem;
            }
            else if (target.ItemType == BabyFunction.Rock)
            {
                targetTransform = RockItem;
            }
            else if (target.ItemType == BabyFunction.Shhh)
            {
                targetTransform = ShhhItem;
            }
            else
            {
                targetTransform = TickleItem;
            }

            target.KillItem(targetTransform);

            if (BabyUIBridge.IsRunning.Value)
            {
                CurrentBaby.ApplyPenalty(target.ItemType);
            }
        }
		#endregion

		#region Private Methods
		#endregion

		#region Public Properties
		#endregion
	}
}