namespace UnityTest
{
    using NUnit.Framework;

    using BabySimulator;

    [TestFixture]
    [Category("Baby Game Description Tests")]
    internal class BabyGameDescriptionTest
    {
        [Test]
        [Category("Basic Generator Tests")]
        public void ConstructorShouldPopulatePropertiesTest()
        {
            var b = new BabyLevelDescription("asdf", "fdsa", 0.2f, 1.3f, 0.4f, -1f);

            Assert.AreEqual("asdf", b.BeforeMessage);
            Assert.AreEqual("fdsa", b.AfterMessage);
            
            Assert.AreEqual(0.2f, b.StartingHunger);
            Assert.AreEqual(1.3f, b.StartingWakefulness);
            Assert.AreEqual(0.4f, b.StartingBoredom);
            Assert.AreEqual(-1f, b.StartingDistress);
        }

        [Test]
        [Category("Basic Generator Tests")]
        public void GeneratedBabyShouldHaveCorrectPropertiesTest()
        {
            var b = new BabyLevelDescription("asdf", "fdsa", 0.2f, 1.3f, 0.4f, -1f);
            var bb = b.ToBaby();
            
            Assert.AreEqual(0.2f, bb.Hunger);
            Assert.AreEqual(1f, bb.Wakefulness);
            Assert.AreEqual(0.4f, bb.Boredom);
            Assert.AreEqual(null, bb.Distress);
        }

        [Test]
        [Category("Basic Generator Tests")]
        public void AttributeShouldReturnActiveIfStartingLevelGreaterThanOrEqualToZeroTest()
        {
            var b1 = new BabyLevelDescription("asdf", "fdsa", 0.2f, -0.3f, 0.4f, -1f);
            
            Assert.IsTrue(b1.HungerActive);
            Assert.IsFalse(b1.WakefulnessActive);
            Assert.IsTrue(b1.BoredomActive);
            Assert.IsFalse(b1.DistressActive);
            
            var b2 = new BabyLevelDescription("asdf", "fdsa", -0.2f, -0.3f, 0.4f, 1f);
            
            Assert.IsFalse(b2.HungerActive);
            Assert.IsFalse(b2.WakefulnessActive);
            Assert.IsTrue(b2.BoredomActive);
            Assert.IsTrue(b2.DistressActive);
        }
    }
}
