namespace UnityTest
{
    using NUnit.Framework;

    using BabySimulator;

    [TestFixture]
    [Category("Baby Tests")]
    internal class BabyTests
    {
        [Test]
        [Category("Basic Attribute Tests")]
        public void ShouldHaveFiveAttributesWhichAreNullIfNotActivatedTest()
        {
            var b = new Baby();
            Assert.AreEqual(null, b.Hunger);
            Assert.AreEqual(null, b.Wakefulness);
            Assert.AreEqual(null, b.Boredom);
            Assert.AreEqual(null, b.Distress);
        }

        [Test]
        [Category("Basic Attribute Tests")]
        public void ActivationShouldSetAttributesToZeroInGetterTest()
        {
            var b = new Baby();
            b.Activate(BabyAttribute.Hunger);
            b.Activate(BabyAttribute.Wakefulness);
            b.Activate(BabyAttribute.Boredom);
            b.Activate(BabyAttribute.Distress);
            
            Assert.AreEqual(0, b.Hunger);
            Assert.AreEqual(1f, b.Wakefulness);
            Assert.AreEqual(0, b.Boredom);
            Assert.AreEqual(0, b.Distress);
        }

        [Test]
        [Category("Basic Attribute Tests")]
        public void ActivateAllShouldActivateAllAttributesTest()
        {
            var b = new Baby();
            b.ActivateAll();
            
            Assert.AreEqual(0, b.Hunger);
            Assert.AreEqual(1f, b.Wakefulness);
            Assert.AreEqual(0, b.Boredom);
            Assert.AreEqual(0, b.Distress);
        }

        [Test]
        [Category("Basic Attribute Tests")]
        public void IsActivationShouldReturnIfAttributeIsActivatedTest()
        {
            var b = new Baby();
            
            Assert.IsFalse(b.IsActive(BabyAttribute.Hunger));
            Assert.IsFalse(b.IsActive(BabyAttribute.Wakefulness));
            Assert.IsFalse(b.IsActive(BabyAttribute.Boredom));
            Assert.IsFalse(b.IsActive(BabyAttribute.Distress));
            
            b.Activate(BabyAttribute.Hunger);
            b.Activate(BabyAttribute.Wakefulness);
            b.Activate(BabyAttribute.Boredom);
            b.Activate(BabyAttribute.Distress);
            
            Assert.IsTrue(b.IsActive(BabyAttribute.Hunger));
            Assert.IsTrue(b.IsActive(BabyAttribute.Wakefulness));
            Assert.IsTrue(b.IsActive(BabyAttribute.Boredom));
            Assert.IsTrue(b.IsActive(BabyAttribute.Distress));
        }
                
        [Test]
        [Category("Basic Attribute Test")]
        public void AttributesShouldBeClamped01Test()
        {
            var b = new Baby();
            b.ActivateAll();

            Assert.IsTrue(b.Hunger <= 1d && b.Hunger >= 0);
            Assert.IsTrue(b.Wakefulness <= 1d && b.Wakefulness >= 0);

            for (var i = 0; i < 1000; ++i)
            {
                b.Rock();
            }

            Assert.IsTrue(b.Hunger <= 1d && b.Hunger >= 0);
            Assert.IsTrue(b.Wakefulness <= 1d && b.Wakefulness >= 0);
        }

        [Test]
        [Category("Basic Attribute Test")]
        public void AttributesShouldBeClamped01WithConstructorTest()
        {
            var b = new Baby(10f, 50f, -100f, 100f);
            b.ActivateAll();
            
            Assert.IsTrue(b.Hunger <= 1f && b.Hunger >= 0);
            Assert.IsTrue(b.Wakefulness <= 1f && b.Wakefulness >= 0);
            Assert.IsTrue(b.Boredom <= 1f && b.Boredom >= 0);
            Assert.IsTrue(b.Distress <= 1f && b.Distress >= 0);
        }
        
        [Test]
        [Category("Basic Attribute Test")]
        public void HungerAbove60PercentShouldIncreaseWakefulnessTest()
        {
            var b2 = new Baby(0.7f, 0, 0, 0);
            b2.ActivateAll();
            b2.Update();
            
            Assert.AreEqual(0.01f, b2.Wakefulness);
        }

        [Test]
        [Category("Basic Attribute Test")]
        public void BoredomAbove60PercentShouldIncreaseWakefulnessTest()
        {
            var b2 = new Baby(0, 0, 0.7f, 0);
            b2.ActivateAll();
            b2.Update();
            
            Assert.AreEqual(0.01f, b2.Wakefulness);
        }

        [Test]
        [Category("User Actions Test")]
        public void FeedShouldReduceHungerAndIncreaseBoredomTest()
        {
            var b = new Baby(0.01f, 0, 0, 0);
            b.ActivateAll();
            b.Feed();

            Assert.AreEqual(0.005f, b.Boredom);
            Assert.AreEqual(0, b.Hunger);
        }

        [Test]
        [Category("User Actions Test")]
        public void RockShouldReduceWakefulnessAndIncreaseHungerTest()
        {
            var b = new Baby(0, 0.01f, 0, 0);
            b.ActivateAll();
            b.Rock();

            Assert.AreEqual(0, b.Wakefulness);
            Assert.AreEqual(0.005f, b.Hunger);
        }

        [Test]
        [Category("User Actions Test")]
        public void ShhhShouldReduceDistressTest()
        {
            var b = new Baby(0, 0, 0, 0.01f);
            b.ActivateAll();
            b.Shhh();

            Assert.AreEqual(0, b.Distress);
        }

        [Test]
        [Category("User Actions Test")]
        public void TickleShouldReduceBoredomTest()
        {
            var b = new Baby(0, 0, 0.01f, 0);
            b.ActivateAll();
            b.Tickle();

            Assert.AreEqual(0, b.Boredom);
        }
        
        [Test]
        [Category("Failure Condition Test")]
        public void ShouldFailByDefaultOnHungerOf100PercentTest()
        {
            var b = new Baby(1f, 0, 0, 0);
            b.Update();
            Assert.IsTrue(b.IsMonster);
        }

        [Test]
        [Category("Failure Condition Test")]
        public void ShouldFailByDefaultOnBoredomOf100PercentTest()
        {
            var b = new Baby(0, 0, 1f, 0);
            b.Update();
            Assert.IsTrue(b.IsMonster);
        }
    }
}
